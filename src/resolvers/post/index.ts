import {
  Arg, Args, Mutation, PubSub, PubSubEngine, Query, Resolver, Root, Subscription, UnauthorizedError,
} from 'type-graphql';
import PostEntity from '../../entities/PostEntity';
import CreatePostInput from '../../inputs/post/CreatePostInput';

const posts: PostEntity[] = [];

@Resolver()
class PostResolver {
  @Query(() => [PostEntity])
  posts(): PostEntity[] {
    return posts;
  }

  @Mutation(() => PostEntity)
  async createPost(@Arg('data') data: CreatePostInput, @PubSub() pubSub: PubSubEngine): Promise<PostEntity> {
    const post = new PostEntity().create(data);

    posts.push(post);

    const payload = post;
    await pubSub.publish('NEWPOST', payload);

    return post;
  }

  @Subscription(() => PostEntity, {
    topics: 'NEWPOST',
  })
  onNewPost(@Root() postPayload: PostEntity, @Arg('self') self: string) {
    if (self !== 'Kalleby') {
      console.error('Sai daqui enxerido');
      throw new UnauthorizedError();
    }

    console.log('Autorizado a receber');

    return postPayload;
  }
}

export default PostResolver;
