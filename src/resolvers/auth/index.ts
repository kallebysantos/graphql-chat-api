import { compare } from 'bcryptjs';
import {
  Arg, Mutation, Resolver, UnauthorizedError,
} from 'type-graphql';
import UserEntity from '../../entities/UserEntity';
import {
  BASE_TIME, ITokenObject, signToken, TOKEN_SECRET,
} from '../../helpers/jwt';

@Resolver()
class AuthResolver {
  @Mutation(() => String)
  async login(@Arg('email') email: string, @Arg('pass') pass: string) {
    const user = await UserEntity.findOneOrFail({ where: { email } });

    const isEqual = await compare(pass, user.password);
    if (!isEqual) throw new UnauthorizedError();

    const tokenPayload: ITokenObject = {
      sub: user.id,
      exp: BASE_TIME * 8,
      payload: {
        email: user.email,
      },
    };

    return signToken(tokenPayload, TOKEN_SECRET);
  }
}

export default AuthResolver;
