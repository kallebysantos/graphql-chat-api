import { NonEmptyArray } from 'type-graphql';
import AuthResolver from './auth';
import FriendResolver from './friend';
import MessageResolver from './message';
import PostResolver from './post';
import UserResolver from './user';

const resolvers: NonEmptyArray<Function> = [
  PostResolver,
  UserResolver,
  MessageResolver,
  FriendResolver,
  AuthResolver,
];

export default resolvers;
