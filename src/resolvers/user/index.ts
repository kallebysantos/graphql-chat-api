import { ApolloError } from 'apollo-server';
import { hash } from 'bcryptjs';
import {
  Arg, Authorized, Ctx, Mutation, Query, Resolver,
} from 'type-graphql';
import IUserToken from '../../auth/interfaces/IUserToken';
import UserEntity from '../../entities/UserEntity';
import CreateUserInput from '../../inputs/user/CreateUserInput';

@Resolver()
class UserResolver {
  @Authorized()
  @Query(() => [UserEntity])
  async users(@Ctx('user') user: IUserToken): Promise<UserEntity[]> {
    console.log(user);

    const users = await UserEntity.find({
      relations: [
        'friends',
        'messagesSent',
        'receivedMessages',
      ],
    });

    return users;
  }

  @Mutation(() => UserEntity)
  async createUser(@Arg('data') data: CreateUserInput): Promise<UserEntity> {
    const wasAlready = await UserEntity.findOne({ where: { email: data.email } });
    if (wasAlready) throw new ApolloError('This email is already in use');

    const user = UserEntity.create(data);
    user.password = await hash(data.password, 8);

    return user.save();
  }
}

export default UserResolver;
