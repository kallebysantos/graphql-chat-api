import { ApolloError } from 'apollo-server';
import {
  Arg, Authorized, Ctx, Mutation, Publisher, PubSub,
  Resolver, Root, Subscription, UnauthorizedError,
} from 'type-graphql';
import IUserToken from '../../auth/interfaces/IUserToken';
import MessageEntity from '../../entities/MessageEntity';
import UserEntity from '../../entities/UserEntity';
import CreateMessageInput from '../../inputs/message/CreateMessageInput';
import { MessageTopics, ReceiveMessageFilter } from './topics';

@Resolver()
class MessageResolver {
  @Authorized()
  @Mutation(() => MessageEntity)
  async sendMessage(
    @Arg('data') data: CreateMessageInput,
    @PubSub(MessageTopics.ReceiveMessage) publish: Publisher<MessageEntity>,
    @Ctx('user') { sub }: IUserToken,
  ): Promise<MessageEntity> {
    const sender = await UserEntity.findOneOrFail(sub, { relations: ['friends'] });
    const target = await UserEntity.findOneOrFail(data.targetId);

    if (!sender.isFriendTo(target)) throw new ApolloError('Invalid target ID');

    const message = MessageEntity.create(data);
    message.sendAt = new Date().toUTCString();
    message.sender = sender;
    message.target = target;
    await message.save();

    await publish(message);

    return message;
  }

  @Authorized()
  @Mutation(() => Boolean)
  async removeMessage(
    @Ctx('user') { sub }: IUserToken,
    @Arg('messageId') messageId: string,
  ): Promise<boolean> {
    const user = await UserEntity.findOneOrFail(sub, { relations: ['messagesSent'] });
    const message = await MessageEntity.findOneOrFail(messageId, { relations: ['sender', 'target'] });

    if (!message.wasSentBy(user)) throw new UnauthorizedError();

    const target = await UserEntity.findOneOrFail(message.target.id, { relations: ['receivedMessages'] });
    user.removeMessageSent(message);
    target.removeReceivedMessage(message);

    await UserEntity.save([user, target]);

    return !!MessageEntity.remove(message);
  }

  @Authorized()
  @Subscription(() => MessageEntity, {
    topics: MessageTopics.ReceiveMessage,
    filter: ({
      payload, context,
    }: ReceiveMessageFilter) => (payload.target.id === context.user?.sub),
  })
  async receiveMessages(@Root() message: MessageEntity):Promise<MessageEntity> {
    return message;
  }
}

export default MessageResolver;
