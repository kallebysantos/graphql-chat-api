import { ResolverFilterData } from 'type-graphql';
import IContext from '../../auth/interfaces/IContext';
import MessageEntity from '../../entities/MessageEntity';

export interface ReceiveMessageFilter extends ResolverFilterData{
  payload: MessageEntity;
  context: IContext;
}

export const MessageTopics = {
  ReceiveMessage: 'RECEIVE_MSG_NOTIFICATION',
};
