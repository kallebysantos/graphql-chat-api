import {
  Arg, Authorized, Ctx, Mutation, Resolver,
} from 'type-graphql';
import IUserToken from '../../auth/interfaces/IUserToken';
import UserEntity from '../../entities/UserEntity';

@Resolver()
class FriendResolver {
  @Authorized()
  @Mutation(() => UserEntity)
  async addFriend(
    @Ctx('user') { sub }: IUserToken, @Arg('friendId') friendId: string,
  ): Promise<UserEntity> {
    const user = await UserEntity.findOneOrFail(sub, { relations: ['friends'] });
    const friend = await UserEntity.findOneOrFail(friendId, { relations: ['friends'] });

    user.appendFriend(friend);
    friend.appendFriend(user);

    await UserEntity.save([user, friend]);

    return user;
  }

  @Authorized()
  @Mutation(() => Boolean)
  async removeFriend(
    @Ctx('user') { sub }: IUserToken, @Arg('friendId') friendId: string,
  ): Promise<boolean> {
    const user = await UserEntity.findOneOrFail(sub, { relations: ['friends'] });
    const friend = await UserEntity.findOneOrFail(friendId, { relations: ['friends'] });

    user.removeFriend(friend);
    friend.removeFriend(user);

    return !!UserEntity.save([user, friend]);
  }
}

export default FriendResolver;
