import { ApolloServer } from 'apollo-server';
import { buildSchema, NonEmptyArray } from 'type-graphql';
import { createConnection } from 'typeorm';
import authChecker from './auth/AuthChecker';
import IContext from './auth/interfaces/IContext';

export async function startServer(resolvers: NonEmptyArray<Function>) {
  const schema = await buildSchema({
    resolvers,
    authChecker,
  });

  const server = new ApolloServer({
    schema,
    context: ({ req, connection }) => {
      const token = (connection) ? connection.context.authorization : req?.headers?.authorization;

      return {
        req,
        token,
      };
    },
  });

  const { url } = await server.listen();
  console.info(`🔥Server listening at ${url}`);

  return server;
}

export async function startDatabase() {
  const connection = await createConnection();
  console.info(`👌Connected with database ${connection.driver.database}`);

  return connection;
}

export default async function bootstrap(resolvers: NonEmptyArray<Function>) {
  await startServer(resolvers);
  await startDatabase();
}
