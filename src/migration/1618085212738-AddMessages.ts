import {MigrationInterface, QueryRunner} from "typeorm";

export class AddMessages1618085212738 implements MigrationInterface {
    name = 'AddMessages1618085212738'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "messages" ("id" varchar PRIMARY KEY NOT NULL, "message" text NOT NULL, "sendAt" date NOT NULL)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "messages"`);
    }

}
