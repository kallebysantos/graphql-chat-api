import {MigrationInterface, QueryRunner} from "typeorm";

export class UserMessagesRelationship1618085959572 implements MigrationInterface {
    name = 'UserMessagesRelationship1618085959572'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "temporary_messages" ("id" varchar PRIMARY KEY NOT NULL, "message" text NOT NULL, "sendAt" date NOT NULL, "senderId" varchar, "targetId" varchar)`);
        await queryRunner.query(`INSERT INTO "temporary_messages"("id", "message", "sendAt") SELECT "id", "message", "sendAt" FROM "messages"`);
        await queryRunner.query(`DROP TABLE "messages"`);
        await queryRunner.query(`ALTER TABLE "temporary_messages" RENAME TO "messages"`);
        await queryRunner.query(`CREATE TABLE "temporary_messages" ("id" varchar PRIMARY KEY NOT NULL, "message" text NOT NULL, "sendAt" date NOT NULL, "senderId" varchar, "targetId" varchar, CONSTRAINT "FK_2db9cf2b3ca111742793f6c37ce" FOREIGN KEY ("senderId") REFERENCES "users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_acb75cb21e718f808a4af6106f4" FOREIGN KEY ("targetId") REFERENCES "users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_messages"("id", "message", "sendAt", "senderId", "targetId") SELECT "id", "message", "sendAt", "senderId", "targetId" FROM "messages"`);
        await queryRunner.query(`DROP TABLE "messages"`);
        await queryRunner.query(`ALTER TABLE "temporary_messages" RENAME TO "messages"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "messages" RENAME TO "temporary_messages"`);
        await queryRunner.query(`CREATE TABLE "messages" ("id" varchar PRIMARY KEY NOT NULL, "message" text NOT NULL, "sendAt" date NOT NULL, "senderId" varchar, "targetId" varchar)`);
        await queryRunner.query(`INSERT INTO "messages"("id", "message", "sendAt", "senderId", "targetId") SELECT "id", "message", "sendAt", "senderId", "targetId" FROM "temporary_messages"`);
        await queryRunner.query(`DROP TABLE "temporary_messages"`);
        await queryRunner.query(`ALTER TABLE "messages" RENAME TO "temporary_messages"`);
        await queryRunner.query(`CREATE TABLE "messages" ("id" varchar PRIMARY KEY NOT NULL, "message" text NOT NULL, "sendAt" date NOT NULL)`);
        await queryRunner.query(`INSERT INTO "messages"("id", "message", "sendAt") SELECT "id", "message", "sendAt" FROM "temporary_messages"`);
        await queryRunner.query(`DROP TABLE "temporary_messages"`);
    }

}
