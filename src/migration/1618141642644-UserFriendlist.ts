import {MigrationInterface, QueryRunner} from "typeorm";

export class UserFriendlist1618141642644 implements MigrationInterface {
    name = 'UserFriendlist1618141642644'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "users_friends_users" ("usersId_1" varchar NOT NULL, "usersId_2" varchar NOT NULL, PRIMARY KEY ("usersId_1", "usersId_2"))`);
        await queryRunner.query(`CREATE INDEX "IDX_a3b73d9dd6e964868c76294b77" ON "users_friends_users" ("usersId_1") `);
        await queryRunner.query(`CREATE INDEX "IDX_6803c4075d7779e2e27d6b14c3" ON "users_friends_users" ("usersId_2") `);
        await queryRunner.query(`DROP INDEX "IDX_a3b73d9dd6e964868c76294b77"`);
        await queryRunner.query(`DROP INDEX "IDX_6803c4075d7779e2e27d6b14c3"`);
        await queryRunner.query(`CREATE TABLE "temporary_users_friends_users" ("usersId_1" varchar NOT NULL, "usersId_2" varchar NOT NULL, CONSTRAINT "FK_a3b73d9dd6e964868c76294b77c" FOREIGN KEY ("usersId_1") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_6803c4075d7779e2e27d6b14c34" FOREIGN KEY ("usersId_2") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, PRIMARY KEY ("usersId_1", "usersId_2"))`);
        await queryRunner.query(`INSERT INTO "temporary_users_friends_users"("usersId_1", "usersId_2") SELECT "usersId_1", "usersId_2" FROM "users_friends_users"`);
        await queryRunner.query(`DROP TABLE "users_friends_users"`);
        await queryRunner.query(`ALTER TABLE "temporary_users_friends_users" RENAME TO "users_friends_users"`);
        await queryRunner.query(`CREATE INDEX "IDX_a3b73d9dd6e964868c76294b77" ON "users_friends_users" ("usersId_1") `);
        await queryRunner.query(`CREATE INDEX "IDX_6803c4075d7779e2e27d6b14c3" ON "users_friends_users" ("usersId_2") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "IDX_6803c4075d7779e2e27d6b14c3"`);
        await queryRunner.query(`DROP INDEX "IDX_a3b73d9dd6e964868c76294b77"`);
        await queryRunner.query(`ALTER TABLE "users_friends_users" RENAME TO "temporary_users_friends_users"`);
        await queryRunner.query(`CREATE TABLE "users_friends_users" ("usersId_1" varchar NOT NULL, "usersId_2" varchar NOT NULL, PRIMARY KEY ("usersId_1", "usersId_2"))`);
        await queryRunner.query(`INSERT INTO "users_friends_users"("usersId_1", "usersId_2") SELECT "usersId_1", "usersId_2" FROM "temporary_users_friends_users"`);
        await queryRunner.query(`DROP TABLE "temporary_users_friends_users"`);
        await queryRunner.query(`CREATE INDEX "IDX_6803c4075d7779e2e27d6b14c3" ON "users_friends_users" ("usersId_2") `);
        await queryRunner.query(`CREATE INDEX "IDX_a3b73d9dd6e964868c76294b77" ON "users_friends_users" ("usersId_1") `);
        await queryRunner.query(`DROP INDEX "IDX_6803c4075d7779e2e27d6b14c3"`);
        await queryRunner.query(`DROP INDEX "IDX_a3b73d9dd6e964868c76294b77"`);
        await queryRunner.query(`DROP TABLE "users_friends_users"`);
    }

}
