import { Field, ID, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  Column, Entity, ManyToOne, PrimaryGeneratedColumn,
} from 'typeorm';
import UserEntity from './UserEntity';

@Entity({ name: 'messages' })
@ObjectType()
class MessageEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id!: string;

  @Column({ type: 'text' })
  @Field(() => String)
  message!: string;

  @Column()
  @Field(() => String)
  sendAt!: string;

  @ManyToOne((type) => UserEntity, (senderUser) => senderUser.messagesSent)
  @Field(() => UserEntity)
  sender!: UserEntity

  @ManyToOne((type) => UserEntity, (targetUser) => targetUser.receivedMessages)
  @Field(() => UserEntity)
  target!: UserEntity;

  wasSentBy(user: UserEntity): Boolean {
    return !!(this.sender.id === user.id);
  }
}

export default MessageEntity;
