import { Field, ID, ObjectType } from 'type-graphql';
import {
  BaseEntity, Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn,
} from 'typeorm';
import MessageEntity from './MessageEntity';

@Entity({ name: 'users' })
@ObjectType()
class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id!: string;

  @Column({ length: 60 })
  @Field(() => String)
  name!: string;

  @Column({ unique: true })
  @Field(() => String)
  email!: string;

  @Column({ length: 60 })
  password!: string;

  @OneToMany((type) => MessageEntity, (msgSent) => msgSent.sender)
  @Field(() => [MessageEntity])
  messagesSent!: [MessageEntity]

  @OneToMany((type) => MessageEntity, (receivedMsg) => receivedMsg.target)
  @Field(() => [MessageEntity])
  receivedMessages!: [MessageEntity]

  @ManyToMany((type) => UserEntity) @JoinTable()
  @Field(() => [UserEntity])
  friends!: UserEntity[];

  getFriend(friendId: string): UserEntity | undefined {
    return this.friends.find((item) => item.id === friendId);
  }

  isFriendTo(user: UserEntity): boolean {
    return !!this.getFriend(user.id);
  }

  appendFriend(friend: UserEntity) {
    if (this.id === friend.id) throw new Error('User and friend can not have the same ID');

    const isAlreadyFriends = this.getFriend(friend.id);
    if (isAlreadyFriends) throw new Error('That user is already on friend list');

    this.friends.push(friend);
  }

  removeFriend(friend: UserEntity) {
    const friendIndex = this.friends.findIndex((item) => item.id === friend.id);
    this.friends.splice(friendIndex, 1);
  }

  removeMessageSent(message: MessageEntity) {
    const messageIndex = this.messagesSent.findIndex((item) => item.id === message.id);
    this.messagesSent.splice(messageIndex, 1);
  }

  removeReceivedMessage(message: MessageEntity) {
    const messageIndex = this.receivedMessages.findIndex((item) => item.id === message.id);
    this.receivedMessages.splice(messageIndex, 1);
  }
}

export default UserEntity;
