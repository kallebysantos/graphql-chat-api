import { v4 } from 'uuid';
import { Field, ID, ObjectType } from 'type-graphql';
import CreatePostInput from '../inputs/post/CreatePostInput';

@ObjectType()
class PostEntity {
  @Field(() => ID)
  id!: string;

  @Field(() => String)
  title!: string;

  @Field(() => String)
  content!: string;

  create(data: CreatePostInput): PostEntity {
    return Object.assign(this, {
      id: v4(),
      ...data,
    });
  }
}

export default PostEntity;
