import { AuthChecker } from 'type-graphql';
import IContext from './interfaces/IContext';
import { TOKEN_SECRET, verifyBearerToken } from '../helpers/jwt';
import IUserToken from './interfaces/IUserToken';

const authChecker: AuthChecker<IContext> = async ({ context }) => {
  const { token } = context;

  if (!token) return false;

  const decoded = await verifyBearerToken(token, TOKEN_SECRET);

  context.user = decoded as IUserToken;

  return !!decoded;
};

export default authChecker;
