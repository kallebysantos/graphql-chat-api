interface IUserToken {
  sub: string;
  exp: string;
  iat: string;
  payload?: Record<string, unknown>
}

export default IUserToken;
