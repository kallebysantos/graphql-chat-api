import { ExpressContext } from 'apollo-server-express';
import IUserToken from './IUserToken';

interface IContext{
  token?: string;
  user?: IUserToken;
}

export default IContext;
