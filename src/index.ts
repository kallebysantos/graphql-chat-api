import 'reflect-metadata';
import dotenv from 'dotenv';
import resolvers from './resolvers';
import bootstrap from './server';

dotenv.config();
bootstrap(resolvers);
