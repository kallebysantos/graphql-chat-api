import dotenv from 'dotenv';
import {
  verify, sign, SignOptions,
} from 'jsonwebtoken';
import { UnauthorizedError } from 'type-graphql';

export interface ITokenObject {
  sub: string,
  exp?: number,
  iat?: number,
  payload: Record<string, unknown>,
}

dotenv.config();

export const TOKEN_SECRET = process.env.TOKEN_SECRET as string;
export const BASE_TIME = 3600;

export async function verifyToken(token: string, secret: string): Promise<object | undefined> {
  return new Promise((resolve, reject) => {
    verify(token, secret, (error, decoded) => (
      (error) ? reject(new UnauthorizedError()) : resolve(decoded)));
  });
}

export async function verifyBearerToken(
  bearerToken: string, secret: string,
): Promise<object | undefined> {
  const [, token] = bearerToken.split(' '); // Removes Bearer word from token

  return verifyToken(token, secret);
}

export async function signToken(data: ITokenObject, secret: string): Promise<string> {
  const options: SignOptions = {
    issuer: 'chat.api',
    subject: data.sub,
    expiresIn: data.exp || '1h',
  };

  return new Promise((resolve, reject) => {
    sign(data.payload, secret, options, (error, encoded) => (
      (encoded) ? resolve(encoded) : reject(new Error(error?.message))
    ));
  });
}
