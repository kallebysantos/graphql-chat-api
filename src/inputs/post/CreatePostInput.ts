import { Length } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
class CreatePostInput {
  @Field()
  @Length(3, 30)
  title!: string;

  @Field()
  @Length(3, 255)
  content!: string;
}

export default CreatePostInput;
