import { IsUUID } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
class AddFriendInput {
  @Field()
  @IsUUID()
  userId!: string;

  @Field()
  @IsUUID()
  friendID!: string;
}

export default AddFriendInput;
