import { IsUUID } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
class CreateMessageInput {
  @Field()
  @IsUUID()
  targetId!: string;

  @Field()
  message!: string;
}

export default CreateMessageInput;
