import { IsEmail, Length } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
class CreateUserInput {
  @Field()
  @Length(3, 60)
  name!: string;

  @Field()
  @IsEmail()
  email!: string;

  @Field()
  @Length(5, 60)
  password!: string;
}

export default CreateUserInput;
